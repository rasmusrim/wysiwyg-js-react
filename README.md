# wysiwyg-js-react
## How to upgrade
1. Bump version in `package.json`
2. Push to `main`

## Example
    import '@ella/wysiwyg-js-react/lib/esm/wysiwyg.scss'
    import { Wysiwyg, WysiwygTemplate } from '@ella/wysiwyg-js-react'

    const wysiwygTemplates: WysiwygTemplate[] = [
      { name: 'Text only', template: 'This is a cool template' },
      { name: 'HTML', template: '<b>bold</b> <i>italic</i> <font color="aqua">color</font>' },
    ]
    
    const references: ReferencesByCategory = [
      {
        name: 'Pending',
        references: [
          {
            id: 1,
            title: 'Title 1',
            authors: 'Author 1',
            year: '2001',
            abstract: 'Abstract 1',
            journal: 'Journal 1',
            published: true,
            pubmed_id: 1,
          },
          {
            id: 1,
            title: 'Title 1',
            authors: 'Author 1',
            year: '2001',
            abstract: 'Abstract 1',
            journal: 'Journal 1',
            published: true,
            pubmed_id: 1,
          },
          {
            id: 1,
            title: 'Title 1',
            authors: 'Author 1',
            year: '2001',
            abstract: 'Abstract 1',
            journal: 'Journal 1',
            published: true,
            pubmed_id: 1,
          },
        ],
      },
      {
        name: 'Evaluated',
        references: [
          {
            id: 2,
            title: 'Title 2',
            authors: 'Author 2',
            year: '2002',
            abstract: 'Abstract 2',
            journal: 'Journal 2',
            published: true,
            pubmed_id: 2,
          },
        ],
      },
    ]
  

    <Wysiwyg
        onChange={(value) => console.log('New value: ', value)}
        templates={wysiwygTemplates}
        references={references}
        placeholder={'Click and start typing...'}
        initialValue={"This is <span style='color: red'>SPARTA</span>"}
        username={'testuser2'}
        onPasteImage={(file) =>
          uploadFile(file).then((id) => {
            const uuid = uuidv4()
            const label = `Attachment ${id} ${uuid}`
            const src = apiConfig.baseUrl + apiConfig.paths.downloadAttachment + id
            return `<img id='${uuid}' src='${src}' alt='${label}' title='${label}'>`
          })
        }
      />
