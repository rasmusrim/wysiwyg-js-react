export class EventListeners {
  constructor() {
    this.eventListeners = []
  }

  add(el, type, func) {
    el.addEventListener(type, func)
    this.eventListeners.push({ element: el, type: type, function: func })
  }

  remove(el, type, func) {
    el.removeEventListener(type, func)
  }

  removeAll() {
    for (let i = 0; i < this.eventListeners.length; i++) {
      let el = this.eventListeners[i]
      this.remove(el.element, el.type, el.function)
    }
  }
}
