import React from 'react'
import { WysiwygJsEditor } from '../../wysiwygjs/WysiwygJsEditor'

interface Props {
  wysiwygEditor: WysiwygJsEditor
}

export function FontColorPopover({ wysiwygEditor }: Props) {
  return (
    <div className="wysiwygfontcolorpopover">
      {wysiwygEditor.fontColors.map((color) => (
        <div
          onClick={() => wysiwygEditor.setFontColor(color)}
          className="color-item"
          style={{ color, borderColor: color }}
          key={color}
        >
          A
        </div>
      ))}
    </div>
  )
}
