import React from 'react'
import { WysiwygJsEditor } from '../../wysiwygjs/WysiwygJsEditor'

interface Props {
  wysiwygEditor: WysiwygJsEditor
}

export function HighlightColorPopover({ wysiwygEditor }: Props) {
  return (
    <div className="wysiwyghighlightcolorpopover">
      {wysiwygEditor.highlightColors.map((color) => (
        <div
          onClick={() => wysiwygEditor.setHighlightColor(color)}
          className="color-item"
          style={{ backgroundColor: color }}
          key={color}
        >
          A
        </div>
      ))}
    </div>
  )
}
