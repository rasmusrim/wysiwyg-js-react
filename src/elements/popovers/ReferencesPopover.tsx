import React from 'react'
import { WysiwygJsEditor } from '../../wysiwygjs/WysiwygJsEditor'
import { ReferencesByCategory } from '../../types/References'

interface Props {
  references: ReferencesByCategory
  wysiwygEditor: WysiwygJsEditor
}

export function ReferencesPopover({ references, wysiwygEditor }: Props) {
  return (
    <div className="wysiwygreferencespopover">
      {references.map((category) => (
        <div>
          <div className="reference-category-title">{category.name}</div>
          {category.references.map((reference) => (
            <div className="reference-item">
              <button
                className="purple reference-button"
                onClick={() => wysiwygEditor.insertReference(reference)}
              >
                +
              </button>
              <div>
                <div
                  className="reference-title"
                  title="{{ $ctrl.formatReference(r, true) }}"
                >
                  {reference.title ?? ''}
                </div>
                <div
                  className="reference-citation"
                  title="{{ $ctrl.formatReference(r, false) }}"
                >
                  {wysiwygEditor.formatReference(reference)}
                </div>
              </div>
            </div>
          ))}
        </div>
      ))}
    </div>
  )
}
