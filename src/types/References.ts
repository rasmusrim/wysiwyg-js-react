export type ReferencesByCategory = { name: string; references: Reference[] }[]

export interface Reference {
  abstract: string
  authors: string
  formatted?: {
    shortDesc: string
  }
  id: number
  journal: string
  published: boolean
  pubmed_id: number
  title: string
  urls?: { [key: string]: string }
  year: string
}
