import { useEffect } from 'react'
import { WysiwygJsEditor } from './wysiwygjs/WysiwygJsEditor'

export function useWysiwygEditor({
  containerRef,
  username,
  onEditorObjectChange,
  onContentChange,
  initialValue,
  disabled,
  onPasteImage,
}) {
  useEffect(() => {
    if (containerRef.current) {
      onEditorObjectChange(
        new WysiwygJsEditor({
          element: containerRef.current,
          username,
          onEditorObjectChange,
          onContentChange,
          initialValue,
          disabled,
          onPasteImage,
        }),
      )
    }
  }, [
    containerRef,
    onEditorObjectChange,
    username,
    onContentChange,
    initialValue,
    disabled,
  ])
}
