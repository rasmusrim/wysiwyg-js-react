import React, { useMemo, useRef, useState } from 'react'
import _ from 'lodash'
import { useWysiwygEditor } from './useWysiwygEditor'
import { Toolbar } from './elements/Toolbar'
import { WysiwygJsEditor } from './wysiwygjs/WysiwygJsEditor'
import { ReferencesByCategory } from './types/References'

export type WysiwygPopoverType =
  | 'fontcolor'
  | 'highlightcolor'
  | 'linkform'
  | 'templates'
  | 'references'

export interface WysiwygTemplate {
  name: string
  template: string
}

interface Props {
  onChange: (content: string) => void
  templates: WysiwygTemplate[]
  references: ReferencesByCategory
  initialValue?: string
  placeholder?: string
  disabled?: boolean
  username: string
  onPasteImage: (file: File) => Promise<string>
}

export function Wysiwyg({
  onChange,
  templates,
  references,
  initialValue = '',
  placeholder = '',
  disabled = false,
  username,
  onPasteImage,
}: Props) {
  const [wysiwygEditor, setWysiwygEditor] = useState<WysiwygJsEditor | null>(null)
  const containerRef = useRef(null)

  useWysiwygEditor({
    containerRef,
    ...useMemo(
      () => ({
        username,
        onEditorObjectChange: (editor) => setWysiwygEditor(_.cloneDeep(editor)),
        onContentChange: onChange,
        initialValue,
        disabled,
        onPasteImage,
      }),
      [disabled, initialValue, onChange, username],
    ),
  })

  return (
    <div className={'wysiwygcontainer'} ref={containerRef}>
      <div className={'wysiwygeditor'} />
      <div className="wysiwygplaceholder" />
      <div className="wysiwygplaceholder" title={placeholder}>
        {placeholder}
      </div>

      <Toolbar
        wysiwygEditor={wysiwygEditor}
        templates={templates}
        references={references}
      />
    </div>
  )
}
